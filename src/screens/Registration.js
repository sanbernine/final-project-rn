import React from 'react';
import {
    AsyncStorage,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
} from 'react-native';
import { connect } from 'react-redux';

import { resetRegister, register } from '../redux/actions';

class RegistrationScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email: '',
            password: ''
        }
    }

    componentDidMount() {
        this.props.resetRegister();
    }

    handleInput = data => this.setState({
        [data.type] : data.text
    });

    handleRegister = () => {
        
        this.props.register(this.state);
        
    }
    
    storeToken = async (value) => await AsyncStorage.setItem('token', value);

    componentDidUpdate(prevProps) {
        if (this.props.reg.email != '') {
            this.props.navigation.goBack();
        }

        if (this.props.reg.errorMessage != prevProps.reg.errorMessage) {
            console.warn(this.props.reg.errorMessage);
        }
    }

    render() {
        return (
            <View style={ styles.container }>
                <Text style={{ marginBottom: 10, fontWeight: 'bold' }}>Registrasi</Text>
                <TextInput
                    onChangeText={text => this.handleInput({ type: 'name', text })}
                    style={ styles.textInput }
                    placeholder='Nama'
                />
                <TextInput
                    onChangeText={ text => this.handleInput({ type: 'email', text }) }
                    style={ styles.textInput }
                    placeholder='Email'
                />
                <TextInput
                    onChangeText={ text => this.handleInput({ type: 'password', text }) }
                    style={ styles.textInput }
                    placeholder='Password'
                    secureTextEntry={true}
                />
                <TouchableOpacity onPress={ () => this.handleRegister() }>
                    <View style={ styles.button }>
                        <Text style={{ color: '#87CDEA' }}>{ this.props.reg.isLoading ? '...' : 'REGISTRASI' }</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textInput: {
        borderWidth: 1,
        borderColor: '#E5E5E5',
        width: 300,
        height: 50,
        borderRadius: 10,
        padding: 10,
        alignItems: 'center',
        textAlign: 'center',
        marginVertical: 5,
    },
    button: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 10,
        backgroundColor: '#003366',
        width: 300,
        borderRadius: 10,
        height: 50
    }
});

const mapStateToProps = (state) => {
    return {
        reg: state.regReducers
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        resetRegister: () => dispatch(resetRegister()),
        register: data => dispatch(register(data))
    }
} 

export default connect(mapStateToProps, mapDispatchToProps) (RegistrationScreen);
