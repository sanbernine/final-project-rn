import React from 'react';
import {
    FlatList,
    StyleSheet,
    Text,
    View,
} from 'react-native';
import { connect } from 'react-redux';

import { NavBar, ScheduleCard } from './components';
import { getSchedules } from '../redux/actions';

class ScheduleScreen extends React.Component {
    componentDidMount() {
        this.props.getSchedules();
    }

    componentDidUpdate(prevProps) {
        if (this.props.reference != prevProps.reference) {
            this.props.getSchedules();
        }
    }

    render() {
        let { user, schedules, venues } = this.props;

        return (
            <View style={ styles.container }>
                <FlatList
                    ListHeaderComponent={
                        <View>
                            <View >
                                    <Text style={{fontSize:20,color:'blue',textAlign:'center'}}>Hai:{ user.name }</Text>
                            </View>
                            <View style={{paddingHorizontal:16}}>
                                <Text style={{fontWeight:'bold',fontSize:20, textAlign:'center'}}>Jadwal Kegiatan</Text>
                            </View>
                        </View>
                    }
                    data={ schedules }
                    renderItem={ schedule =>
                        <ScheduleCard
                            schedule={ schedule.item }
                            navigation={ this.props.navigation }
                            venue={ venues.filter(venue => venue.id == schedule.item.venue_id) }
                        />
                    }
                    keyExtractor={ item => item.id.toString() }
                />

                <NavBar navigation={this.props.navigation}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        paddingTop: 35,
    },
    row: {
        paddingVertical: 16,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems:'center',
        marginBottom: 10,
    },
    box: {
        flex: 1,
        height: 200,
        marginRight:10,
        alignItems:'center',
        justifyContent:'center'
    },
    box2: {
        backgroundColor: 'green',
        borderRadius:200
    },
});

const mapStateToProps = state => {
    return {
        user: state.scheduleReducers.user,
        schedules: state.scheduleReducers.schedules,
        reference: state.joinReducers.reference,
        venues: state.venueReducers.venues
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getSchedules: () => dispatch(getSchedules())
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (ScheduleScreen);
