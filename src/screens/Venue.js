import React from 'react';
import {
    FlatList,
    Image,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import { connect } from 'react-redux';

import { NavBar, BookingCard } from './components';
import { showVenue } from '../redux/actions';

class VenueScreen extends React.Component {
    componentDidMount() {
        this.props.showVenue(this.props.route.params.venueId);
    }

    addBooking = () => this.props.navigation.navigate('AddBooking', { venue: this.props.venue });

    render() {
        let venue = this.props.venue;
        
        return (
            <View style={ styles.container }>  
                <FlatList
                    ListHeaderComponent={
                        <View>
                            <Image source={{ uri: `https://mainbersama.demosanbercode.com${venue.thumbnail}` }} style={styles.image} />

                            <View style={{ padding: 16 }}>
                                <Text style={ styles.title }>{ venue.name }</Text>

                                <Text style={ styles.subTitle }>Lokasi</Text>
                                <Text style={ styles.text }>{ venue.location }</Text>

                                <Text style={ styles.subTitle }>Jam buka</Text>
                                <Text style={ styles.text }>{ venue.open_hour }</Text>
                                
                                <Text style={ styles.subTitle }>Harga</Text>
                                <Text style={ styles.text }>{ venue.rate }</Text>

                                <Text style={ styles.subTitle }>Daftar booking</Text>
                            </View>
                        </View>
                    }
                    data={ venue.bookings }
                    renderItem={ booking => <BookingCard booking={booking.item} navigation={this.props.navigation} />}
                    keyExtractor={ item => item.id.toString() }
                    ListFooterComponent={
                        <TouchableOpacity onPress={ () => this.addBooking() } style={ styles.button }>
                            <Text style={ styles.buttonText }>Tambah Booking</Text>
                        </TouchableOpacity>
                    }
                />

                <NavBar navigation={ this.props.navigation }/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    image: {
        height: 300,
        width: '100%',
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold'
    },
    subTitle: {
        fontSize: 16,
        fontWeight:'bold',
        color:'#616161',
        paddingTop: 16
    },
    text: {
        fontSize: 14,
        color: '#616161',
        paddingTop: 8
    },
    button: {
        backgroundColor:'#61A756',
        alignSelf:'center',
        padding: 12,
        borderRadius: 4,
        margin: 16
    },
    buttonText: {
        fontSize: 13,
        fontWeight: 'bold',
        color: 'white',
        textAlign: 'center',
    }
});

const mapStateToProps = state => {
    return {
        venue: state.venueReducers.venue
    }
}

const mapDispatchToProps = dispatch => {
    return {
        showVenue: venueId => dispatch(showVenue(venueId))
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (VenueScreen);
