import React from 'react';
import {
    AsyncStorage,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
} from 'react-native';
import { connect } from 'react-redux';

import { login } from '../redux/actions';

class LoginScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: ''
        }
    }

    handleInput = data => this.setState({
        [data.type] : data.text
    });

    handleLogin = () => this.props.login(this.state);
    
    toRegistration = () => this.props.navigation.navigate('Registration');

    storeToken = async (value) => await AsyncStorage.setItem('token', value);

    componentDidUpdate(prevProps) {
        if (this.props.auth.token != prevProps.auth.token) {
            this.storeToken(this.props.auth.token);
        }

        if (this.props.reg.email != '' && prevProps.reg.email == '') {
            this.setState({
                email: this.props.reg.email,
                password: ''
            });       
        }

        if (this.props.auth.errorMessage != prevProps.auth.errorMessage) {
            console.warn(this.props.auth.errorMessage);
        }
    }

    render() {
        return (
            <View style={ styles.container }>
                <Text style={{ marginBottom: 10, fontWeight: 'bold' }}>Login</Text>
                <TextInput
                    onChangeText={ text => this.handleInput({ type: 'email', text }) }
                    style={ styles.textInput }
                    placeholder='Email'
                    value={ this.state.email }
                />
                <TextInput
                    onChangeText={ text => this.handleInput({ type: 'password', text }) }
                    style={ styles.textInput }
                    placeholder='Password'
                    secureTextEntry={true}
                />
                <TouchableOpacity onPress={ () => this.handleLogin() }>
                    <View style={ styles.button }>
                        <Text style={{ color: '#87CDEA' }}>{ this.props.auth.isLoading ? '...' : 'LOGIN' }</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={ () => this.toRegistration() }>
                    <Text style={{ color: '#757575' }}>Registrasi</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textInput: {
        borderWidth: 1,
        borderColor: '#E5E5E5',
        width: 300,
        height: 50,
        borderRadius: 10,
        padding: 10,
        alignItems: 'center',
        textAlign: 'center',
        marginVertical: 5,
    },
    button: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 10,
        backgroundColor: '#003366',
        width: 300,
        borderRadius: 10,
        height: 50
    }
});

const mapStateToProps = (state) => {
    return {
        auth: state.authReducers,
        reg: state.regReducers,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        login: data => dispatch(login(data))
    }
} 

export default connect(mapStateToProps, mapDispatchToProps) (LoginScreen);
