import React from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

class ScheduleCard extends React.Component {
    viewBooking = (bookingId) => this.props.navigation.navigate('Booking', { bookingId: bookingId });

    render() {
        const { schedule, venue } = this.props;

        return (
           
            <View style={styles.container}>
                <View style={{ flex:1 }}>
                    <Text style={{fontSize:20,fontWeight:'bold',color:'red'}}>{ venue[0].name }</Text>
                    <Text>Tanggal Main : { schedule.play_date }</Text>
                    <Text>Jam Main : { schedule.start }</Text>
                </View>
                <TouchableOpacity onPress={ () => this.viewBooking(schedule.pivot.booking_id) } style={ styles.button }>
                    <Icon name='view-list' size={25} style={ styles.serachIcon } />
                </TouchableOpacity>
            </View>
            
        );
    }
};

const styles = StyleSheet.create({
    container: {
        marginHorizontal: 16,
        marginVertical: 5,
        padding: 16,
        backgroundColor: '#F5F5F5',
        flexDirection: 'row'
    },
    button: {
        alignItems:'center',
        justifyContent:'center',
        padding: 4,
        borderRadius: 8
    },
    buttonText: {
        fontSize: 13,
        fontWeight: 'bold',
        color: 'white',
        textAlign: 'center'
    }
});

export default ScheduleCard;
