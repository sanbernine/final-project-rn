import React from 'react';
import {
    Image,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';

class VenueCard extends React.Component {
    viewVenue = venueId => this.props.navigation.navigate('Venue', { venueId: venueId });

    render() {
        const venue = this.props.venue;

        return (
            <View style={styles.container}>
                <Image source={{ uri: `https://mainbersama.demosanbercode.com${venue.thumbnail}` }} style={styles.image} />
                <Text style={ styles.title }>{ venue.name }</Text>
                <Text style={ styles.address }>{ venue.location }</Text>
                <TouchableOpacity onPress={ () => this.viewVenue(venue.id) } style={ styles.button }>
                    <Text style={ styles.buttonText }>Lihat</Text>
                </TouchableOpacity>
            </View>
        );
    }
};

const styles = StyleSheet.create({
    container: {
        padding: 16
    },
    image: {
        height: 200,
        width: '100%',
        borderRadius: 8,
    },
    title: {
        paddingTop: 8,
        fontSize: 16,
        fontWeight: 'bold'
    },
    address: {
        paddingTop: 4,
        fontSize: 14,
        color: '#7A7A7A',
    },
    button: {
        backgroundColor:'#61A756',
        padding: 12,
        alignSelf: 'flex-end',
        borderRadius: 4
    },
    buttonText: {
        fontSize: 13,
        fontWeight: 'bold',
        color: 'white',
        textAlign: 'center'
    }
});

export default VenueCard;
