import React from 'react';
import {
    AsyncStorage,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';

import { saveToken } from '../../redux/actions'

class NavBar extends React.Component {
    home = () => this.props.navigation.navigate('Home');
    
    schedule = () => this.props.navigation.navigate('Schedule');

    logout = () => {
        AsyncStorage.removeItem('token');
        this.props.saveToken('');
    }

    render() {
        return (
            <View style={ styles.navBar }>
                <TouchableOpacity style={ styles.tabItem } onPress={ () => this.home() }>
                    <Icon name='home' size={25} />
                    <Text style={ styles.tabTitle }>Home</Text>
                </TouchableOpacity>
                <TouchableOpacity style={ styles.tabItem } onPress={ () => this.schedule() }>
                    <Icon name='clipboard-text-outline' size={25} />
                    <Text style={ styles.tabTitle }>Jadwal Main</Text>
                </TouchableOpacity>
                <TouchableOpacity style={ styles.tabItem } onPress={ () => this.logout() }>
                    <Icon name='logout-variant' size={25} />
                    <Text style={ styles.tabTitle }>Logout</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    navBar: {
        backgroundColor: 'white',
        height: 60,
        borderTopWidth: 0.5,
        borderColor: '#E5E5E5',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    tabItem: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    tabTitle: {
        fontSize: 11,
        color: '#3c3c3c',
        paddingTop: 4
    }
});

const mapDispatchToProps = (dispatch) => {
    return {
        saveToken: (token) => dispatch(saveToken(token))
    }
}

export default connect(null, mapDispatchToProps) (NavBar);
