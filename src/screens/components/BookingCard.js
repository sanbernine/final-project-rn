import React from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';

class BookingCard extends React.Component {
    viewBooking = (bookingId) => this.props.navigation.navigate('Booking', { bookingId: bookingId });

    render() {
        const booking = this.props.booking;

        return (
            <View style={styles.container}>
                <Text>{ booking.play_date } • { booking.start }</Text>
                <Text>Durasi: { booking.duration }</Text>
                <Text>Jumlah pemain: { booking.total_players }</Text>
                <TouchableOpacity onPress={ () => this.viewBooking(booking.id) } style={ styles.button }>
                    <Text style={ styles.buttonText }>Lihat</Text>
                </TouchableOpacity>
            </View>
        );
    }
};

const styles = StyleSheet.create({
    container: {
        marginHorizontal: 16,
        marginVertical: 4,
        padding: 16,
        backgroundColor: '#F5F5F5'
    },
    title: {
        paddingTop: 8,
        fontSize: 16,
        fontWeight: 'bold'
    },
    address: {
        paddingTop: 4,
        fontSize: 14,
        color: '#7A7A7A',
    },
    button: {
        backgroundColor:'#61A756',
        padding: 12,
        alignSelf: 'flex-end',
        borderRadius: 4
    },
    buttonText: {
        fontSize: 13,
        fontWeight: 'bold',
        color: 'white',
        textAlign: 'center'
    }
});

export default BookingCard;
