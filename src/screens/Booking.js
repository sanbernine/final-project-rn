import React from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Image
} from 'react-native';
import { connect } from 'react-redux';

import { NavBar } from './components';
import {
    showBooking,
    joinPlay,
    unjoinPlay
} from '../redux/actions';
import { ScrollView } from 'react-native-gesture-handler';

class BookingScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isJoined: '',
        }
    }

    componentDidMount() {
        this.props.showBooking(this.props.route.params.bookingId);
    }

    checkJoined = () => {
        const meOnList = this.props.players.filter(player => player.id == this.props.userId);
        this.setState({ isJoined: meOnList.length > 0 ? true : false })
    };

    joinPlay = () => {
        this.state.isJoined
            ? this.props.unjoinPlay(this.props.route.params.bookingId)
            : this.props.joinPlay(this.props.route.params.bookingId);
       
        this.props.navigation.goBack();
    }

    componentDidUpdate(prevProps) {
        if (this.props.players != prevProps.players) {
            this.checkJoined();
        }
    }

    render() {
        let { booking, players, venue } = this.props;

        return (
            <View style= {{ flex: 1 }}>
                <View style={ styles.container }>  
                <ScrollView>
                <Image source={{ uri: `https://mainbersama.demosanbercode.com${venue.thumbnail}` }} style={styles.image} />
                    <Text style={ styles.title }>{ venue.name }</Text>
                    <Text style={ styles.text }>Alamat:{ venue.location }</Text>
                    <Text>Tanggal: { booking.play_date }</Text>
                    <Text>Jam: { booking.start }</Text>
                    <Text>Durasi: { booking.duration }</Text>
                    <Text>Jumlah pemain: { booking.total_players }</Text>

                    <Text style={ styles.title1 }>Daftar pemain</Text>
                    
                    { players.map((player, index) => <Text key={index}>{index+1}. {player.name}</Text>) }    
                    
                    
                    <TouchableOpacity onPress={ () => this.joinPlay() } style={ styles.button }>
                        <Text style={ styles.buttonText }>{ this.state.isJoined ? 'Unjoin' : 'Join' }</Text>
                    </TouchableOpacity>
                    </ScrollView>
                </View>

                <NavBar navigation={this.props.navigation}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        paddingHorizontal: 16,
        paddingTop: 35,
    },
    image: {
        height: 300,
        width: '100%',
        borderRadius: 16,
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
        paddingTop: 16
    },
    title1: {
        fontSize: 20,
        fontWeight: 'bold',
        paddingTop: 16,
        color:'red'
    },
    subTitle: {
        fontSize: 16,
        fontWeight:'bold',
        color:'#616161',
        paddingTop: 16
    },
    text: {
        fontSize: 14,
        color: '#616161',
        paddingTop: 8
    },
    button: {
        backgroundColor:'#61A756',
        alignSelf:'center',
        padding: 12,
        borderRadius: 4,
        margin: 16
    },
    buttonText: {
        fontSize: 13,
        fontWeight: 'bold',
        color: 'white',
        textAlign: 'center',
    },
    lineSeparator: {
        height: 0.5,
        backgroundColor: '#E5E5E5'
    }
});

const mapStateToProps = state => {
    return {
        booking: state.bookingReducers.booking,
        players: state.bookingReducers.players,
        venue: state.bookingReducers.venue,
        userId: state.scheduleReducers.user.id
    }
}

const mapDispatchToProps = dispatch => {
    return {
        showBooking: bookingId => dispatch(showBooking(bookingId)),
        joinPlay: bookingId => dispatch(joinPlay(bookingId)),
        unjoinPlay: bookingId => dispatch(unjoinPlay(bookingId))
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (BookingScreen);
