import React from 'react';
import {
    FlatList,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';

import { NavBar, VenueCard } from './components';
import { getVenues } from '../redux/actions';

class HomeScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            venues: [],
            search: '',
            categories: [1, 2, 3, 4]
        }
    }

    componentDidMount() {
        this.props.getVenues();
        this.filterVenue();
    }

    handleSearch = text => this.setState({
        search: text.toLowerCase()
    });

    handleCategory = categoryId => {
        const categories = this.state.categories;
        
        this.setState({
            categories: categories.includes(categoryId)
                ? categories.filter(item => item != categoryId)
                : [...categories, categoryId]
        });
    }

    filterVenue = () => {
        const filteredVenue = this.state.search != ''
            ? this.props.venues.filter(item =>
                item.name.toLowerCase().includes(this.state.search)
                &&
                this.state.categories.includes(item.category_id)
            )
            : this.props.venues.filter(item =>
                this.state.categories.includes(item.category_id)
            );

        this.setState({ venues: filteredVenue });
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.venues != prevProps.venues ||
            this.state.search != prevState.search ||
            this.state.categories != prevState.categories)
        {
            this.filterVenue();
        }
    }
    
    render() {
        let { categories, venues } = this.state;        

        return (
            <View style={ styles.container }>
                <View style={ styles.searchContainer }> 
                    <TextInput
                        placeholder='cari lapangan'
                        onChangeText={ text => this.handleSearch(text) }
                        style={ styles.searchInput }
                    />
                    <Icon name='magnify' size={25} style={ styles.serachIcon } />
                </View>

                <View style={ styles.categoryContainer }>
                    <TouchableOpacity
                        onPress={ () => this.handleCategory(1) }
                        style={ categories.includes(1) ? styles.categorySelected : styles.categoryUnselected }
                    >
                        <Text style={ styles.categoryText }>Futsal</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={ () => this.handleCategory(2) }
                        style={ categories.includes(2) ? styles.categorySelected : styles.categoryUnselected }
                    >
                        <Text style={ styles.categoryText }>Sepak Bola</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={ () => this.handleCategory(3) }
                        style={ categories.includes(3) ? styles.categorySelected : styles.categoryUnselected }
                    >
                        <Text style={ styles.categoryText }>Basket</Text>
                    </TouchableOpacity>
                    
                    <TouchableOpacity
                        onPress={ () => this.handleCategory(4) }
                        style={ categories.includes(4) ? styles.categorySelected : styles.categoryUnselected }
                    >
                        <Text style={ styles.categoryText }>Voli</Text>
                    </TouchableOpacity>
                </View>

                <FlatList
                    style= {{ marginTop: 8 }}
                    data={ venues }
                    renderItem={ venue => <VenueCard venue={venue.item} navigation={this.props.navigation} />}
                    keyExtractor={ item => item.id.toString() }
                    ItemSeparatorComponent={ () => <View style={ styles.lineSeparator } />}
                />

                <NavBar navigation={this.props.navigation}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        paddingTop: 35
    },
    searchContainer: {
        flexDirection: 'row',
        marginHorizontal: 16
    },
    searchInput: {
        flex: 1,
        borderWidth: 1,
        borderColor: '#E8E8E8',
        borderRadius: 20,
        height: 40,
        fontSize: 13,
        paddingLeft: 45,
        paddingRight: 20
    },
    serachIcon: {
        position: 'absolute',
        top: 8,
        left: 12
    },
    categoryContainer: {
        backgroundColor: 'white',
        height: 40,
        marginTop: 8,
        paddingHorizontal: 10,
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    categorySelected: {
        backgroundColor: '#81C784',
        alignItems: 'center',
        justifyContent: 'center',
        width: 80,
        borderWidth: 1,
        borderColor: '#EFEFEF',
        borderRadius: 10
    },
    categoryUnselected: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 80,
        borderWidth: 1,
        borderColor: '#EFEFEF',
        borderRadius: 10
    },
    categoryText: {
        fontSize: 11,
        color: '#3c3c3c',
    },
    lineSeparator: {
        height: 0.5,
        backgroundColor: '#E5E5E5'
    }
});

const mapStateToProps = state => {
    return {
        venues: state.venueReducers.venues
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getVenues: () => dispatch(getVenues())
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (HomeScreen);
