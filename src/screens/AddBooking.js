import React from "react";
import {
    Picker,
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
    Image
} from 'react-native';
import DatePicker from 'react-native-datepicker'
import { connect } from 'react-redux';

import { NavBar } from './components';
import { postBooking } from '../redux/actions';

class BookingScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            play_date: '',
            start: '',
            duration: '',
            total_players: '',
            category_id: this.props.route.params.venue.category_id,
        }
    }
    postBooking = () => {
        this.props.postBooking(this.props.route.params.venue.id, this.state);
        this.props.navigation.goBack();
        this.props.navigation.goBack();
    }

    render() {
        const venue = this.props.route.params.venue;

        return (
            <View style= {{ flex: 1 ,paddingHorizontal:2,paddingHorizontal:2}}>
                <ScrollView>
                <View style={ styles.container }>  
                    <Text style={ styles.title1 }>{ venue.name }</Text>

                    <Text style={ styles.subTitle }>Lokasi</Text>
                    <Text style={ styles.text }>{ venue.location }</Text>

                    <Text style={ styles.subTitle }>Harga</Text>
                    <Text style={ styles.text }>{ venue.rate }</Text>

                    <Text style={ styles.subTitle }>Tanggal</Text>
                    <DatePicker
                        style={{width: 200,fontSize:14,fontWeight:'normal',marginBottom:11}}
                        ref={ picker => this.datePicker = picker }
                        date={ this.state.play_date }
                        mode='date'
                        placeholder='tanggal'
                        format='YYYY-MM-DD'
                        minDate='2020-01-01'
                        maxDate='2020-12-12'
                        confirmBtnText='OK'
                        cancelBtnText='Cancel'
                        onDateChange={ date => this.setState({ play_date: date })}
                    />

                    <Text style={ styles.subTitle }>Jam</Text>
                    <Picker
                        selectedValue={ this.state.start }
                        style={{ height: 50, width: 150 }}
                        onValueChange={ (itemValue, itemIndex) => this.setState({ start: itemValue }) }
                    >
                        <Picker.Item label='pilih' />
                        <Picker.Item label={ venue.open_hour } value='1' />
                        <Picker.Item label='jam selanjutnya' value='2' />
                    </Picker>

                    <Text style={ styles.subTitle }>Durasi</Text>
                    <View style={{flexDirection:'row',flex:1}}> 
                    <TextInput
                        keyboardType='number-pad'
                        onChangeText={ text => this.setState({ duration: text }) }
                        style={ styles.textInput }
                    />
                    <Text>/jam</Text>
                    </View>
                    <Text style={ styles.subTitle }>Jumlah pemain</Text>
                    <View style={{flexDirection:'row',flex:1}}>
                    <TextInput
                        keyboardType='number-pad'
                        onChangeText={ text => this.setState({ total_players: text }) }
                        style={ styles.textInput }
                    />
                    <Text style={{width:'30%'}}>/orang</Text>
                    </View>
                    <TouchableOpacity onPress={ () => this.postBooking() } style={ styles.button }>
                        <Text style={ styles.buttonText }>Booking</Text>
                    </TouchableOpacity>
                </View>
                </ScrollView>

                <NavBar navigation={ this.props.navigation }/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        paddingHorizontal: 16,
        paddingTop: 35,
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
        paddingTop: 16
    },
    title1: {
        fontSize: 30,
        fontWeight: 'bold',
        paddingTop: 16,
        color:'red',
        marginBottom:10,
        
    },
    subTitle: {
        fontSize: 16,
        fontWeight:'bold',
        color:'#616161',
        paddingTop: 16
    },
    text: {
        fontSize: 14,
        color: '#616161',
        paddingTop: 8
    },
    textInput: {
        borderWidth: 1,
        borderColor: '#E5E5E5',
        width: 300,
        height: 50,
        borderRadius: 10,
        padding: 10,
        alignItems: 'center',
        textAlign: 'center',
        marginVertical: 5
    },
    button: {
        backgroundColor:'#61A756',
        alignSelf:'center',
        padding: 12,
        borderRadius: 4,
        margin: 16
    },
    buttonText: {
        fontSize: 13,
        fontWeight: 'bold',
        color: 'white',
        textAlign: 'center',
    },
    lineSeparator: {
        height: 0.5,
        backgroundColor: '#E5E5E5'
    }
});

const mapDispatchToProps = dispatch => {
    return {
        postBooking: (venueId, data) => dispatch(postBooking(venueId, data))
    }
}

export default connect(null, mapDispatchToProps) (BookingScreen);
