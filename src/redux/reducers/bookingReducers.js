import {
    BOOKING_REQUEST,
    BOOKING_SUCCESS,
    BOOKING_GET_SUCCESS,
    BOOKING_ERROR
} from '../actionTypes';

const initialState = {
    booking: {},
    players: [],
    venue: {},
    isLoading: false,
    isError: false,
    errorMessage: ''
}

export default (state = initialState, {type, payload}) => {
    switch (type) {
        case BOOKING_REQUEST:
            return {
                ...state,
                booking: {},
                players: [],
                venue: {},
                isLoading: true
            }
        case BOOKING_SUCCESS:
            return {
                ...state,
                isLoading: false
            }
        case BOOKING_GET_SUCCESS:
            return {
                ...state,
                booking: payload,
                players: payload.players,
                venue: payload.venue,
                isLoading: false
            }
        case BOOKING_ERROR:
            return {
                ...state,
                isLoading: false,
                isError: true,
                errorMessage: payload
            }
        default:
            return state;
    }
}
