import {
    SCHEDULE_REQUEST,
    SCHEDULE_SUCCESS,
    SCHEDULE_ERROR
} from '../actionTypes';

const initialState = {
    user: {},
    schedules: [],
    isLoading: false,
    isError: false,
    errorMessage: ''
}

export default (state = initialState, {type, payload}) => {
    switch (type) {
        case SCHEDULE_REQUEST:
            return {
                ...state,
                user: {},
                schedules: [],
                isLoading: true
            }
        case SCHEDULE_SUCCESS:
            return {
                ...state,
                user: payload,
                schedules: payload.play_schedules,
                isLoading: false
            }
        case SCHEDULE_ERROR:
            return {
                ...state,
                isLoading: false,
                isError: true,
                errorMessage: payload
            }
        default:
            return state;
    }
}
