import {
    SAVE_TOKEN,
    LOGIN_SUCCESS,
    LOGIN_REQUEST,
    LOGIN_ERROR
} from '../actionTypes';

const initialState = {
    token: '',
    isLoading: false,
    isError: false,
    errorMessage: ''
}

export default (state = initialState, {type, payload}) => {
    switch (type) {
        case SAVE_TOKEN:
            return {
                ...state,
                token: payload
            }
        case LOGIN_REQUEST:
            return {
                ...state,
                isLoading: true
            }
        case LOGIN_SUCCESS:
            return {
                ...state,
                isLoading: false,
                token: payload
            }
        case LOGIN_ERROR:
            return {
                ...state,
                isLoading: false,
                isError: true,
                errorMessage: payload
            }
        default:
            return state
    }
}
