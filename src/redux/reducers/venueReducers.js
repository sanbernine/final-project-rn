import {
    VENUE_REQUEST,
    VENUE_SUCCESS,
    VENUE_ALL_SUCCESS,
    VENUE_ERROR
} from '../actionTypes';

const initialState = {
    venue: {},
    venues: [],
    isLoading: false,
    isError: false,
    errorMessage: ''
}

export default (state = initialState, {type, payload}) => {
    switch (type) {
        case VENUE_REQUEST:
            return {
                ...state,
                venue: '',
                isLoading: true
            }
        case VENUE_SUCCESS:
            return {
                ...state,
                venue: payload,
                isLoading: false
            }
        case VENUE_ALL_SUCCESS:
            return {
                ...state,
                venues: payload,
                isLoading: false
            }
        case VENUE_ERROR:
            return {
                ...state,
                isLoading: false,
                isError: true,
                errorMessage: payload
            }
        default:
            return state;
    }
}
