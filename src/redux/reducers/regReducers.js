import {
    REGISTER_RESET,
    REGISTER_REQUEST,
    REGISTER_SUCCESS,
    REGISTER_ERROR
} from '../actionTypes';

const initialState = {
    email: '',
    isLoading: false,
    isError: false,
    errorMessage: ''
}

export default (state = initialState, {type, payload}) => {
    switch (type) {
        case REGISTER_RESET:
            return {
                ...state,
                email: ''
            }
        case REGISTER_REQUEST:
            return {
                ...state,
                isLoading: true
            }
        case REGISTER_SUCCESS:
            return {
                ...state,
                email: payload,
                isLoading: false
            }
        case REGISTER_ERROR:
            return {
                ...state,
                isLoading: false,
                isError: true,
                errorMessage: payload
            }
        default:
            return state;
    }
}
