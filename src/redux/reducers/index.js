import { combineReducers } from 'redux';

import authReducers from './authReducers';
import regReducers from './regReducers';
import venueReducers from './venueReducers';
import bookingReducers from './bookingReducers';
import scheduleReducers from './scheduleReducers';
import joinReducers from './joinReducers';

export default combineReducers({
    authReducers,
    regReducers,
    venueReducers,
    bookingReducers,
    scheduleReducers,
    joinReducers
 });
