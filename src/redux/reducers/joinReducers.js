import {
    JOIN_REQUEST,
    JOIN_SUCCESS,
    JOIN_ERROR
} from '../actionTypes';

const initialState = {
    reference: 0,
    isLoading: false,
    isError: false,
    errorMessage: ''
}

export default (state = initialState, {type, payload}) => {
    switch (type) {
        case JOIN_REQUEST:
            return {
                ...state,
                isLoading: true
            }
        case JOIN_SUCCESS:
            return {
                ...state,
                reference: state.reference + 1,
                isLoading: false
            }
        case JOIN_ERROR:
            return {
                ...state,
                isLoading: false,
                isError: true,
                errorMessage: payload
            }
        default:
            return state;
    }
}
