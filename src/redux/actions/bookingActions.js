import Axios from 'axios';

import {
    BOOKING_REQUEST,
    BOOKING_SUCCESS,
    BOOKING_GET_SUCCESS,
    BOOKING_ERROR
} from '../actionTypes';
import { BaseUrl } from '../../constants';

export const showBooking = bookingId => {
    return (dispatch, getState) => {
        dispatch({ type: BOOKING_REQUEST });

        Axios({
            method: 'GET',
            url: `${BaseUrl}/bookings/${bookingId}`,
            headers: {
                Accept: 'application/json',
                Authorization: `Bearer ${getState().authReducers.token}`
            }
        })
        .then(({ data }) => {
            if (data.status) dispatch({ type: BOOKING_GET_SUCCESS, payload: data.booking });
            else dispatch({ type: BOOKING_ERROR, payload: data.message });
        })
        .catch(error => dispatch({ type: BOOKING_ERROR, payload: error.message }));
    }
}

export const postBooking = (venueId, data) => {
    return (dispatch, getState) => {
        dispatch({ type: BOOKING_REQUEST });
        Axios({
            method: 'POST',
            url: `${BaseUrl}/venues/${venueId}/book`,
            headers: {
                Accept: 'application/json',
                Authorization: `Bearer ${getState().authReducers.token}`
            },
            data: data
        })
        .then(({ data }) => {
            if (data.status) dispatch({ type: BOOKING_SUCCESS, payload: data.booking });
            else dispatch({ type: BOOKING_ERROR, payload: data.message });
        })
        .catch(error => dispatch({ type: BOOKING_ERROR, payload: error.message }));
    }
}