import Axios from 'axios';

import {
    REGISTER_RESET,
    REGISTER_REQUEST,
    REGISTER_SUCCESS,
    REGISTER_ERROR
} from '../actionTypes';
import { BaseUrl } from '../../constants';

export const resetRegister = () => {
    return { type: REGISTER_RESET }
}

export const register = data => {
    return dispatch => {
        dispatch({ type: REGISTER_REQUEST });

        Axios({
            method: 'POST',
            url: `${BaseUrl}/register`,
            headers: {
                Accept: 'application/json'
            },
            data: data
        })
        .then(({ data }) => {
            if (data.success) dispatch({ type: REGISTER_SUCCESS, payload: data.data.email });
            else dispatch({ type: REGISTER_ERROR, payload: data.message });
            console.warn(data)
        })
        .catch(error => dispatch({ type: REGISTER_ERROR, payload: error.message }));
    }
}
