import Axios from 'axios';

import {
    VENUE_REQUEST,
    VENUE_SUCCESS,
    VENUE_ALL_SUCCESS,
    VENUE_ERROR
} from '../actionTypes';
import { BaseUrl } from '../../constants';

export const getVenues = () => {
    return (dispatch, getState) => {
        dispatch({ type: VENUE_REQUEST });

        Axios({
            method: 'GET',
            url: `${BaseUrl}/venues`,
            headers: {
                Accept: 'application/json',
                Authorization: `Bearer ${getState().authReducers.token}`
            }
        })
        .then(({ data }) => {
            if (data.status) dispatch({ type: VENUE_ALL_SUCCESS, payload: data.venues });
            else dispatch({ type: VENUE_ERROR, payload: data.message });
        })
        .catch(error => dispatch({ type: VENUE_ERROR, payload: error.message }));
    }
}

export const showVenue = venueId => {
    return (dispatch, getState) => {
        dispatch({ type: VENUE_REQUEST });

        Axios({
            method: 'GET',
            url: `${BaseUrl}/venues/${venueId}`,
            headers: {
                Accept: 'application/json',
                Authorization: `Bearer ${getState().authReducers.token}`
            }
        })
        .then(({ data }) => {
            if (data.status) dispatch({ type: VENUE_SUCCESS, payload: data.venue });
            else dispatch({ type: VENUE_ERROR, payload: data.message });
        })
        .catch(error => dispatch({ type: VENUE_ERROR, payload: error.message }));
    }
}