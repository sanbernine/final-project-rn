import Axios from 'axios';

import {
    SCHEDULE_REQUEST,
    SCHEDULE_SUCCESS,
    SCHEDULE_ERROR
} from '../actionTypes';
import { BaseUrl } from '../../constants';

export const getSchedules = () => {
    return (dispatch, getState) => {
        dispatch({ type: SCHEDULE_REQUEST });

        Axios({
            method: 'GET',
            url: `${BaseUrl}/schedules`,
            headers: {
                Accept: 'application/json',
                Authorization: `Bearer ${getState().authReducers.token}`
            }
        })
        .then(({ data }) => {
            if (data.status) dispatch({ type: SCHEDULE_SUCCESS, payload: data.data });
            else dispatch({ type: SCHEDULE_ERROR, payload: data.message });
        })
        .catch(error => dispatch({ type: SCHEDULE_ERROR, payload: error.message }));
    }
}
