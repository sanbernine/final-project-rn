import Axios from 'axios';

import {
    SAVE_TOKEN,
    LOGIN_REQUEST,
    LOGIN_SUCCESS,
    LOGIN_ERROR,
} from '../actionTypes';
import { BaseUrl } from '../../constants';

export const saveToken = (token) => {
    return { type: SAVE_TOKEN, payload: token }
}

export const login = data => {
    return dispatch => {
        dispatch({ type: LOGIN_REQUEST });

        Axios({
            method: 'POST',
            url: `${BaseUrl}/login`,
            headers: {
                Accept: 'application/json'
            },
            data: data
        })
        .then(({ data }) => {
            if (data.success) dispatch({ type: LOGIN_SUCCESS, payload: data.token });
            else dispatch({ type: LOGIN_ERROR, payload: data.message });
        })
        .catch(error => dispatch({ type: LOGIN_ERROR, payload: error.message }));
    }
}
