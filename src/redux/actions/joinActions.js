import Axios from 'axios';

import {
    JOIN_REQUEST,
    JOIN_SUCCESS,
    JOIN_ERROR
} from '../actionTypes';
import { BaseUrl } from '../../constants';

export const joinPlay = bookingId => {
    return (dispatch, getState) => {
        dispatch({ type: JOIN_REQUEST });

        Axios({
            method: 'POST',
            url: `${BaseUrl}/bookings/${bookingId}/join`,
            headers: {
                Accept: 'application/json',
                Authorization: `Bearer ${getState().authReducers.token}`
            }
        })
        .then(({ data }) => {
            if (data.status) dispatch({ type: JOIN_SUCCESS });
            else dispatch({ type: JOIN_ERROR, payload: data.message });
        })
        .catch(error => dispatch({ type: JOIN_ERROR, payload: error.message }));
    }
}

export const unjoinPlay = bookingId => {
    return (dispatch, getState) => {
        dispatch({ type: JOIN_REQUEST });

        Axios({
            method: 'POST',
            url: `${BaseUrl}/bookings/${bookingId}/unjoin`,
            headers: {
                Accept: 'application/json',
                Authorization: `Bearer ${getState().authReducers.token}`
            }
        })
        .then(({ data }) => {
            if (data.status) dispatch({ type: JOIN_SUCCESS });
            else dispatch({ type: JOIN_ERROR, payload: data.message });
        })
        .catch(error => dispatch({ type: JOIN_ERROR, payload: error.message }));
    }
}