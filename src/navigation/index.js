import React from 'react';
import { AsyncStorage } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { connect } from 'react-redux';

import {
    LoginScreen,
    RegistrationScreen,
    HomeScreen,
    ScheduleScreen,
    VenueScreen,
    BookingScreen,
    AddBooking
} from '../screens';
import { saveToken } from '../redux/actions';

const Stack = createStackNavigator();

class Navigation extends React.Component {
    checkToken = async () => {
        try {
            let token = await AsyncStorage.getItem('token');
            return token;
        } catch (error) {
            return null;
        }
    }

    async componentDidMount() {
        let token = await this.checkToken();
        token !== null ? this.props.saveToken(token) : this.props.saveToken('');
    }

    render() {
        const token = this.props.auth.token;

        return token === ''
        ? (
            <NavigationContainer>
                <Stack.Navigator>
                    <Stack.Screen name='Login' component={ LoginScreen } options={{ headerShown: false }} />
                    <Stack.Screen name='Registration' component={ RegistrationScreen } options={{ headerShown: false }} />
                </Stack.Navigator>
            </NavigationContainer>
        )
        : (
            <NavigationContainer>
                <Stack.Navigator>
                    <Stack.Screen name='Home' component={ HomeScreen } options={{ headerShown: false }} />
                    <Stack.Screen name='Schedule' component={ ScheduleScreen } options={{ headerShown: false }} />
                    <Stack.Screen name='Venue' component={ VenueScreen } options={{ headerShown: false }} />
                    <Stack.Screen name='Booking' component={ BookingScreen } options={{ headerShown: false }} />
                    <Stack.Screen name='AddBooking' component={ AddBooking } options={{ headerShown: false }} />
                </Stack.Navigator>
            </NavigationContainer>
        )
    }
} 

const mapStateToProps = state => {
    return {
        auth: state.authReducers
    }
}

const mapDispatchToProps = dispatch => {
    return {
        saveToken: (token) => dispatch(saveToken(token))
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (Navigation);
